#!/bin/sh

set -e # Exit immediately if a command exits with a non-zero status.

envsubst < /etc/nginx/default.conf/tpl > /etc/nginx/conf.d/default.conf
# Docker recommends to run the primary application in the foreground
# so all logs will be printed to the console
nginx -g 'daemon off;'  
